import firebase_admin.credentials
import firebase_admin.firestore
import functools
import json
import numpy as np
import os
import requests
import shutil
from collections import defaultdict
from google.cloud.firestore_v1beta1.document import DocumentSnapshot
# from tqdm import tqdm
from rasterio import features
from tqdm import tqdm

# from tqdm import tqdm
from raster_mask_module.label.labeledObjects import LabeledPoly
from raster_mask_module.settings import logging
from raster_mask_module.utils import save_polys_as_json, save_image, gsutil_ls

logger = logging.getLogger(__name__)


def _multi_in(text, substrings):
    for sub_str in substrings:
        assert isinstance(sub_str, tuple)
        if sub_str[0] in text.split("/") and sub_str[1] in text.split("/"):
            return True
    return False


class LabelDbManager:
    _APP_NAME = 'seetree-merger-tool'
    _SLEEP_INTERVAL = 2 ** 13
    _SLEEP_TIME = 1.0
    _HTTP_TIMEOUT = 12400

    def __init__(self, user=None, service_account=None):
        self._SERVICE_ACCOUNT = service_account or os.environ['FIREBASE_SERVICE_ACCOUNT']
        self._USER = user or "SEETREE"
        self.index = dict()
        self._image_path_filter_fn = lambda x: x
        self._path_record_filter_fn = lambda x: x
        try:
            firebase_cred = firebase_admin.credentials.Certificate(self._SERVICE_ACCOUNT)
            self._firebase_app = firebase_admin.initialize_app(name=self._USER,
                                                               credential=firebase_cred,
                                                               options={'httpTimeout': self._HTTP_TIMEOUT})
            firestore = firebase_admin.firestore.client(app=self._firebase_app)
            self._images_collection = firestore.collection('images')
            self._path_collection = firestore.collection('paths')
            self._mask_collection = firestore.collection('masks')
        except requests.exceptions.HTTPError as e:
            logging.info(e)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.closeFirebase()

    def _get_path_record(self, doc: DocumentSnapshot):
        try:
            rec = json.loads(doc.get('json'))
            if len(rec) != 2:
                raise (ValueError('bad record {}'.format(rec)))
            return rec[1]
        except KeyError:
            return None

    def _filter_image_doc(self, doc: DocumentSnapshot):
        try:
            if doc.id is None:
                return False
            path = doc.get('path')
            return self._image_path_filter_fn(path)
        except KeyError:
            return False

    def getImages(self):
        return self._images_collection

    def getPaths(self):
        return self._path_collection

    def setImageFilter(self, filter_fun):
        if filter_fun is None:
            self._image_path_filter_fn = lambda x: x
        else:
            self._image_path_filter_fn = filter_fun

    def setRecordFilter(self, filter_fun):
        if filter_fun is None:
            self._path_record_filter_fn = lambda x: x
        else:
            self._path_record_filter_fn = filter_fun

    def _buildIndex(self):
        index = dict()
        try:
            for i, doc in enumerate(self._images_collection.where('status', '==', 'done').get()):
                if self._filter_image_doc(doc):
                    if doc.id in index:
                        raise (KeyError("Duplicate Key: {}".format(doc.id)))
                    index[doc.id] = {'path': doc.get('path'),
                                     'id': doc.id}
            logging.info("\nready: found {} image records".format(len(index)))
            self.index = index

        except:
            logging.info('fail')
            self._buildIndex()

    def setImagePathFilter(self, names: list):
        if names:
            self.setImageFilter(functools.partial(_multi_in,
                                                  substrings=names))
        else:
            self.setImageFilter(None)

    def getFilteredObjectsIter(self):
        self._buildIndex()
        for key, image_doc in self.index.items():
            path_query = self._path_collection.where('imageID', '==', key).get()
            for path_doc in path_query:
                record = self._get_path_record(path_doc)
                yield image_doc, record
        logging.info("")

    def getFilteredLabels(self, names=None):
        self.setImagePathFilter(names)
        self.setRecordFilter(None)
        images = dict()
        total = 0
        used = 0
        for image_record, path_record in tqdm(self.getFilteredObjectsIter()):
            image_id = image_record['id']
            images.setdefault(image_id, image_record).setdefault('objects', []).append(path_record)
            used += 1
            total += 1
        logging.info("\ndone: scanned {} path records, used {} records".format(total, used))
        return list(images.values())

    def closeFirebase(self):
        firebase_admin.delete_app(self._firebase_app)


def getFramePolys(labels, clipName):
    named_polys = defaultdict(list)
    clipBase = os.path.splitext(os.path.basename(clipName))[0]
    for v in labels:
        base, ext = os.path.splitext(os.path.basename(v["path"]))
        if base == clipBase:
            for obj in v["objects"]:
                if 'segments' in obj.keys():
                    if len(obj['segments']) > 4:
                        polygon = LabeledPoly.fromLabeler(obj)
                        named_polys['all'].append(polygon.polygon)
                        if 'data' in obj.keys():
                            named_polys[str(obj['data']['name']).strip().lower()].append(polygon.polygon)
    return named_polys


def get_images_from_folder(root_folder, folder_content, serviceAcount=None):
    """
    Takes a list of images from a folder and uploads all existing masks from DB
    :param root_folder: local folder for uploaded files and output folder
    :param folder_content: list of file names raster .jpg tiles
    :return:
    """
    folders_tile_list = {}
    for folder_ in folder_content:
        if not folder_.endswith('/'):
            continue
        logging.info(folder_)
        current_folder_name = folder_.split('/')[-2]
        image_folder_path = os.path.join(root_folder, current_folder_name)
        os.makedirs(image_folder_path, exist_ok=True)
        folders_tile_list[current_folder_name] = gsutil_ls(folder_)
        f_path_lst = folders_tile_list[current_folder_name]
        try:
            tile_top_left_coord = [f_path.split('/')[-1].split('.')[0].split('_')[-2:]
                                   for f_path in f_path_lst if f_path.endswith('jpg')]

            tile_top_left_coord = np.array([(int(coord[0]), int(coord[1])) for coord in tile_top_left_coord])

            x_sorted = np.sort(np.unique(tile_top_left_coord[:, 0]))
            y_sorted = np.sort(np.unique(tile_top_left_coord[:, 1]))
        except IndexError:
            logger.error("Error reading tile coordinates from file name")
            continue
        x_min = np.min(np.diff(x_sorted))
        y_min = np.min(np.diff(y_sorted))
        tile_shape = (x_min, y_min)
        imgs_names = []
        for file_path in folders_tile_list[current_folder_name]:
            if file_path.lower().endswith(".jpg") or file_path.lower().endswith(".png"):
                tile_file_name = file_path.split('/')[-1]  # .split('.')[0]
                imgs_names.append(tile_file_name)

        imgs_filtered = [(i, current_folder_name) for i in imgs_names]
        fm = LabelDbManager(service_account=serviceAcount)
        labels = fm.getFilteredLabels(imgs_filtered)
        fm.closeFirebase()
        if len(labels) == 0:
            logging.info('Sorry, no labels was found')
        else:
            logging.info(len(labels))
            names = [label['path'] for label in labels]
            folder = os.path.join(root_folder, current_folder_name)
            os.makedirs(folder, exist_ok=True)
            rt_folder = os.path.dirname(folder)

            polys_folder_ = os.path.join(rt_folder, current_folder_name, "polys")
            save_mask = os.path.join(rt_folder, current_folder_name, "nrg_masks")
            # if os.path.isdir(polys_folder_):
            #     shutil.rmtree(polys_folder_)
            os.makedirs(polys_folder_, exist_ok=True)
            for name in names:
                clipName_full = name.split('/')[-1]
                clipName_base = clipName_full.split(".")[0]
                # f = folder.split('/')[-1]
                clipName = os.path.join(folder, clipName_full)
                named_polys = getFramePolys(labels, clipName)
                poly_folders = named_polys.keys()
                for poly_fldr in poly_folders:
                    json_name = "_all_" + clipName_base.split('/')[-1] + '.geojson'
                    poly_fldr_path = os.path.join(polys_folder_, poly_fldr)
                    os.makedirs(poly_fldr_path, exist_ok=True)
                    json_path = os.path.join(poly_fldr_path, json_name)

                    poly_shp = named_polys[poly_fldr]
                    save_polys_as_json(poly_shp, json_path)

                    im_size = tile_shape
                    if len(poly_shp) != 0:
                        mask = features.rasterize(shapes=poly_shp,
                                                  out_shape=im_size)
                    else:
                        mask = np.zeros(im_size, np.uint8)
                    # !!!!!!!!!!!!

                    # mask = mask_for_polygons(poly_shp, clipName)
                    file_name = clipName_base.split('/')[-1] + '.png'

                    os.makedirs(save_mask, exist_ok=True)
                    named_mask_save_folder = os.path.join(save_mask, poly_fldr)
                    os.makedirs(save_mask, exist_ok=True)
                    os.makedirs(named_mask_save_folder, exist_ok=True)
                    save_path = os.path.join(named_mask_save_folder, file_name)
                    save_image(mask, save_path)
            logging.info('Masks downloaded')


def get_labels(root_folder, folder, img_type='nrg', mask_dir='nrg_masks'):
    img_dir = '{}/{}/{}'.format(root_folder, folder, img_type)
    mask_dir = '{}/{}/{}'.format(root_folder, folder, mask_dir)
    img_out = '{}/{}/{}_labeled'.format(root_folder, folder, img_type)
    os.makedirs(img_out, exist_ok=False)
    for image in os.listdir(mask_dir):
        img_path = os.path.join(img_dir, image.replace('nrg', img_type).replace('.png', '.jpg'))
        if not os.path.isfile(img_path):
            img_path = os.path.join(img_dir, image.replace('nrg', img_type).replace('.png', '.JPG'))
        img_out_path = os.path.join(img_out, image.replace('nrg', img_type).replace('.png', '.jpg'))
        shutil.copyfile(img_path, img_out_path)
