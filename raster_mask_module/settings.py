import os
import logging
from logging.config import dictConfig


LOGGING_CONFIG = dict({
    'version': 1,
    'disable_existing_loggers': True,  # this fixes the problem
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
            'datefmt': ' %I:%M:%S '
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'

        },
    },
    'loggers': {
        'root': {
            'handlers': ['default'],
            'level': 'INFO',
        },

    },
    'root': {
        'handlers': ['default'],
        'level': 'INFO',
    },

})


logging.config.dictConfig(LOGGING_CONFIG)

DEFAULT_MODEL_VERSION_NON_ALIGNED = "v4"

WORK_ROOT = os.getenv('WORK_ROOT', '/tmp')
