import cv2
import geojson
import json
import numpy as np
import subprocess
from shapely.geometry import mapping


def save_polys_as_json(polys, name):
    # gc = GeometryCollection(polys)
    geoms = {}
    geoms['features'] = []
    geoms['crs'] = {'properties': {'name': 'urn:ogc:def:crs:EPSG::32611'},
                    'type': 'name'}
    geoms['type'] = 'FeatureCollection'
    for i in range(len(polys)):
        geom_in_geojson = geojson.Feature(geometry=mapping(polys[i]), properties={})
        geoms['features'].append(geom_in_geojson)
    with open(name, 'w') as dst:
        # json.dumps()
        json.dump(geoms, dst)


def mask_for_polygons(polygons, im_path):
    """Convert a polygon or multipolygon list back to
       an image mask ndarray"""
    img = cv2.imread(im_path)
    im_size = (img.shape[0], img.shape[1])
    img_mask = np.zeros(im_size, np.uint8)
    if not polygons:
        return img_mask
    # function to round and convert to int
    int_coords = lambda x: np.array(x).round().astype(np.int32)
    exteriors = [int_coords(poly.exterior.coords) for poly in polygons]
    interiors = [int_coords(pi.coords) for poly in polygons
                 for pi in poly.interiors]
    # cv2.fillPoly(img_mask, exteriors, 1)
    # cv2.fillPoly(img_mask, interiors, 0)
    # Why one of them not work always!
    [cv2.fillConvexPoly(img_mask, ext, 1) for ext in exteriors]
    [cv2.fillConvexPoly(img_mask, inter, 0) for inter in interiors]
    return img_mask


def save_image(img, im_path):
    """Saves img mask"""
    cv2.imwrite(im_path, img * 255)


def gsutil_ls(remote_path, multiprocess=False):
    """
    Gsutil list file
    """
    cli_params = ['gsutil', 'ls', remote_path]
    proc = subprocess.Popen(cli_params, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1)
    output, err = proc.communicate("", 10)
    input_folder_browes = output.decode().split("\n")
    return input_folder_browes
