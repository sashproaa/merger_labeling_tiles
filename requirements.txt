certifi==2018.10.15
chardet==3.0.4
idna==2.6
numpy==1.15.4
Pillow==5.3.0
requests==2.18.4
urllib3==1.22
tqdm==4.20.0
rasterio==1.0.24
opencv-python==3.4.0.12
firebase-admin==2.10.0
geopandas==0.3.0
geojson == 2.5.0
Shapely==1.6.2.post1
opencv-contrib-python==3.4.1.15

