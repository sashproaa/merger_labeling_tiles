from shapely.geometry import Polygon


class LabeledRect:
    label_type = 'rectangle'

    def __init__(self, name, left, right, bottom, top):
        self.label_text = name
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top
        self.rgb = None
        self.depth = None

    def setRgbFromArray(self, rgb_array):
        left = int(self.left)
        right = int(self.right)
        bottom = int(self.bottom)
        top = int(self.top)
        self.rgb = rgb_array[top:bottom, left:right]

    def setDepthFromArray(self, depth_array):
        left = int(self.left)
        right = int(self.right)
        bottom = int(self.bottom)
        top = int(self.top)
        self.depth = depth_array[top:bottom, left:right]

    # classmethod to init from object module
    @classmethod
    def fromLabeler(cls, object):
        name = object.get("data", {}).get("name")
        segments = object["segments"]
        left = min(x[0] for x in segments)
        right = max(x[0] for x in segments)
        top = min(x[1] for x in segments)
        bottom = max(x[1] for x in segments)
        return cls(name, left, right, bottom, top)

    def __str__(self):
        return str(
            "name: {}, (l,r,b,t): ({},{},{},{})".format(self.label_text, self.left, self.right, self.bottom, self.top))


class LabeledPoly:
    label_type = 'polygon'

    def __init__(self, name, polygon, left, right, top, bottom):
        self.polygon = polygon
        self.label_text = name
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top
        self.rgb = None
        self.depth = None

    def setRgbFromArray(self, rgb_array):
        left = int(self.left)
        right = int(self.right)
        bottom = int(self.bottom)
        top = int(self.top)
        self.rgb = rgb_array[top:bottom, left:right]

    def setDepthFromArray(self, depth_array):
        left = int(self.left)
        right = int(self.right)
        bottom = int(self.bottom)
        top = int(self.top)
        self.depth = depth_array[top:bottom, left:right]

    # classmethod to init from object module
    @classmethod
    def fromLabeler(cls, object):
        name = object.get("data", {}).get("name")
        segments = object["segments"]
        left = min(x[0] for x in segments)
        right = max(x[0] for x in segments)
        top = min(x[1] for x in segments)
        bottom = max(x[1] for x in segments)
        polygon = Polygon(segments)
        return cls(name, polygon, left, right, top, bottom)


    def __str__(self):
        return str(
            "name: {}, (l,r,b,t): ({},{},{},{})".format(self.label_text, self.left, self.right, self.bottom, self.top))