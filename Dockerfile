FROM gcr.io/google-appengine/python
RUN apt-get update

RUN apt-get update
RUN apt-get install -y libproj-dev gdal-bin libgdal-dev python-gdal python3-gdal libsm6 libxext6 libfontconfig1 libxrender1 blt-dev

RUN apt-get install -y curl lsb-release
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-jessie main" |  tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg |  apt-key add -
RUN apt-get update
RUN apt-get install -y google-cloud-sdk
RUN apt-get install -y gcc python-dev python-setuptools
RUN echo y | python2.7 -m pip uninstall crcmod
RUN python2.7 -m pip install -U crcmod

RUN apt-get install -y  python-numpy-dev
RUN apt-get install -y wkhtmltopdf

# Create a virtualenv for dependencies. This isolates these packages from
# system-level packages.
RUN virtualenv /env -p python3.6

# Setting these environment variables are the same as running
# source /env/bin/activate.
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH
ENV PATH /usr/include/gdal:$PATH

# Copy the application's requirements.txt and run pip to install all
# dependencies into the virtualenv.
ADD requirements.txt /app/requirements.txt
ADD requirements-private.txt /app/requirements-private.txt
RUN pip install -r /app/requirements.txt
RUN pip install -i https://seetree:suntreat2017@seetree-pip.appspot.com/pypi -r /app/requirements-private.txt

# Add the application source code.
ADD . /app

