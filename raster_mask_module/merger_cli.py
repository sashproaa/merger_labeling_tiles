import argparse
import cv2
import json
import numpy as np
import os
import rasterio
import seetree.gsutil as gsutil
import tqdm
import uuid
import shutil
from os import listdir
from os.path import isfile
from seetree.microservice_utils.execution_wrapper import Executor
from typing import Tuple

import raster_mask_module.label.labelDbManager as lDbM
from raster_mask_module.utils import gsutil_ls

try:
    WORK_ROOT = os.environ['WORK_ROOT']
except:
    WORK_ROOT = '/tmp'

geo_json_name_endswith = '_meta.json'


def check_window_boundary(tile_window: Tuple[Tuple, Tuple], raster_shape: Tuple):
    """
    Check whether tile window in raster window
    Otherwise crop tile window
    :return:
    """
    x_right = min(tile_window[0][1], raster_shape[0])
    y_bottom = min(tile_window[1][1], raster_shape[1])
    tile_window = ((tile_window[0][0], x_right), (tile_window[1][0], y_bottom))
    return tile_window


def run_merger(args, workpath, logger):
    # download_file(os.path.join(args.input_folder, '*'), workpath)
    folder_content = browse_remote_folder(args.input_folder)

    meta_json_file_path = get_raster_meta_json(folder_content)
    download_file(meta_json_file_path, workpath)

    lDbM.get_images_from_folder(root_folder=workpath, folder_content=folder_content)

    meta_json_file_name = meta_json_file_path.split('/')[-1]

    raster_json_path = os.path.join(workpath, meta_json_file_name)
    geojson_file = open(raster_json_path, 'r')
    geojson_raw_data = geojson_file.read()
    raster_meta = json.loads(geojson_raw_data)
    transform = [float(params.strip()) for params in
                 raster_meta['transform'].replace("|", ',').replace("\n", "").split(r",") if params]
    raster_meta['transform'] = transform[:-3]
    logger.info("meta {}".format(raster_meta))

    mask_raster_type = "int16"
    mask_meta = dict(raster_meta)
    mask_meta['count'] = 1
    mask_meta['dtype'] = mask_raster_type
    mask_meta['nodata'] = -1
    mask_meta['compress'] = 'packbits'
    sub_dirs = [dir for dir in os.listdir(workpath) if os.path.isdir(dir)]

    with lDbM.LabelDbManager() as fm:
        for dirs in sub_dirs:
            output_folder = os.path.join(workpath, dirs, 'output')
            os.makedirs(output_folder, exist_ok=True)
            mask_tile_folder = os.path.join(workpath, dirs, 'nrg_masks')
            if not os.path.isdir(mask_tile_folder):
                continue

            # output_raster_path = os.path.join(output_folder, "raster.tif")
            # tile_folder = os.path.join(workpath, dirs)
            # tiles_file_list = [f for f in listdir(tile_folder) if isfile(os.path.join(tile_folder, f)) and f.endswith('.jpg')]

            # with rasterio.open(output_raster_path, mode='w+', **raster_meta) as output_raster:
            #     for file_name in tiles_file_list:
            #         print(os.path.join(tile_folder, file_name))
            #         img = cv2.imread(os.path.join(tile_folder, file_name))
            #         start_col, start_row = file_name.replace('.jpg', "").split('_')[-2:]
            #         for i in range(raster_meta['count']):
            #             output_raster.write(img[:,:,i], window = ((int(start_row), img.shape[1]+int(start_row)), (int(start_col), img.shape[0]+int(start_col))), indexes=i+1)

            output_raster_by_name = {}
            for named_mask_folder in listdir(mask_tile_folder):
                mask_raster_file_name = f"{named_mask_folder}_mask_raster.tif"
                mask_raster_path = os.path.join(output_folder, mask_raster_file_name)

                with rasterio.open(mask_raster_path, mode='w+', **mask_meta) as mask_raster:

                    named_mask_folder_path = os.path.join(mask_tile_folder, named_mask_folder)
                    if not os.path.isdir(named_mask_folder_path):
                        continue

                    mask_tiles_file_list = [f for f in listdir(named_mask_folder_path) if
                                            isfile(os.path.join(named_mask_folder_path, f)) and f.endswith('.png')]

                    tq = tqdm.tqdm(len(mask_tiles_file_list))
                    for file_name in mask_tiles_file_list:
                        img = cv2.imread(os.path.join(named_mask_folder_path, file_name))

                        start_col, start_row = file_name.replace('.png', "").split('_')[-2:]
                        tq.update(1)
                        if not np.max(img[:, :, 0]) > 0:
                            continue
                        start_row = int(start_row)
                        start_col = int(start_col)
                        tile_window = ((start_row, img.shape[1] + start_row),
                                       (start_col, img.shape[0] + start_col))
                        raster_shape = mask_raster.shape
                        tile_window_checked = check_window_boundary(tile_window, raster_shape)
                        img = img[:tile_window_checked[0][1] - tile_window_checked[0][0],
                              :tile_window_checked[1][1] - tile_window_checked[1][0], 0]

                        mask_raster.write(img.astype(mask_raster_type), window=tile_window_checked, indexes=1)
                        output_raster_by_name[named_mask_folder] = mask_raster_file_name
                    logger.info(f"{named_mask_folder} mask raster created")

            output_path = args.output_path or os.path.join(args.input_folder,
                                                           meta_json_file_name.replace('_meta.json', ""))
            try:
                uploadResult(output_folder, output_path)
                # pass
            except RuntimeError as error:
                logger.error(f"{error}")
                exit(-1)

            fm._mask_collection.add({"path": output_path, "name": meta_json_file_name,
                                         "type": "mask_raster" })
            logger.info(f"Mask raster added to DB with {meta_json_file_name} name")


def uploadResult(upload_path, dst):
    logging.info("upload to {} ".format(dst))
    gsutil.cp(os.path.join(upload_path, '*'), dst)


def browse_remote_folder(remote_path):
    return gsutil_ls(remote_path)


def get_raster_meta_json(input_folder):
    meta_json_end_with = "_meta.json"
    if len(input_folder) > 0:
        for path in input_folder:
            if path.endswith(meta_json_end_with):
                return path
    logger.error("There is no raster meta.json in remote folder. Check path")
    exit(-1)


def download_file(remote_file_path, file_dst_path):
    gsutil.cp(remote_file_path, file_dst_path)


if __name__ == '__main__':
    import logging

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_folder", help="Path to raster_list",
                        type=str, required=True)
    # parser.add_argument("-f", "--farmid", help="farmID", type=str, required=True)
    # parser.add_argument("-f", "--groveid", help="groveID", type=str, required=True)

    parser.add_argument("-o", "--output_path", help="Path to output folder",
                        type=str, required=False, default=None)

    args = parser.parse_args()
    workpath = os.path.join(WORK_ROOT, str(uuid.uuid4()))

    os.makedirs(workpath, exist_ok=True)
    os.chdir(workpath)
    try:
        input_args = vars(args)
        executor = Executor(input_args=input_args)
        executor(run_merger, args=args, workpath=workpath, logger=logger)
    finally:
        shutil.rmtree(workpath)
        # pass
