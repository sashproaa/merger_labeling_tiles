import os
import subprocess
import json
import argparse
import logging

logging.basicConfig(level=logging.INFO)

"""
when sending the job you get a jobId in the response, for example job-alignment-32d3b714
you can request jobsManager for status : http://proxy.seetree.ai/jobs/status/{jobid}?token=5e43296de082d58afd5aa0a0c80daacd
you can request jobsManager for recent logs (this works only while the job is running or around that time): https://proxy.seetree.ai/jobs/logs/{jobid}?token=5e43296de082d58afd5aa0a0c80daacd
you can request logs url for future use, this gets you url for the gcp logging system (this works only while the job is running or around that time, it takes some time from the time you send the job to when the url is ready): https://proxy.seetree.ai/jobs/log_link?job_id={jobid}&token=5e43296de082d58afd5aa0a0c80daacd
"""

"""
prod token 5e43296de082d58afd5aa0a0c80daacd
dev token 987512bd-d117-4441-a8a5-bc5b469bad99
"""


def read_json(path):
    with open(path) as json_data:
        d = json.load(json_data)
    return d


if __name__ == '__main__':
    output_path = os.path.join(os.getcwd(), "_job_list.json")
    regression_jobs_path = os.path.join(os.getcwd(), "regression_jobs")
    dev_token = '987512bd-d117-4441-a8a5-bc5b469bad99'

    parser = argparse.ArgumentParser(description='run alignment jobs')
    parser.add_argument('--email', required=False, default=None, help='status notification email')
    parser.add_argument('--build_id', required=True, help='build ID  which you are going to lunch the job')
    parser.add_argument("--test_path", required=False, default=regression_jobs_path, help="path to test job files")
    parser.add_argument("--token", required=False, default=dev_token, help="token to run job with")
    parser.add_argument("-o", required=False, default=output_path, help="job list output file")
    parser.add_argument("--file_name", required=False, default=None, help="file name? that contains job")
    parser.add_argument("--add", required=False, default=False, help="add job to current job list")
    args = parser.parse_args()

    root_folder = args.test_path
    output_path = args.o
    if not os.path.exists(root_folder):
        raise ValueError("path {} does not exists".format(root_folder))

    files = os.listdir(root_folder)
    files = [os.path.join(root_folder, file) for file in files if file.endswith(".txt")]
    if args.file_name:
        for f_path in files:
            if f_path.find(args.file_name) > 0:
                files = [f_path]
                break
    num_files = len(files)
    job_list = {}
    for num, f in enumerate(files):
        payload = read_json(f)
        file_name = f.split('/')[-1]
        try:
            payload['version'] = args.build_id
            payload['run_on_preemptible'] = False
            if args.email:
                payload['params']['email'] = args.email
            else:
                payload['params']['email'] = None
        except KeyError as err:
            logging.error("There is no such key in dict: {}".format(err))
            continue
        e = subprocess.run(
            ['curl',
             '-X', 'POST',
             'http://proxy.seetree.ai/jobs?token={}'.format(args.token),
             '-H', 'Content-Type:application/json',
             '--data', '{}'.format(json.dumps(payload))
             ], shell=False, stdout=subprocess.PIPE)
        response_data = (e.stdout).decode('utf8')
        if e.returncode == 0 and str(response_data).rfind('job-alignment') == 0:
            job_id = response_data
            job_list[job_id] = payload

        logging.info('job file:  {}\njob ID: {}'.format(file_name, e.stdout))
    file_mode = "w+"
    with open(output_path, file_mode) as job_file:
        job_file.write(json.dumps(job_list))
